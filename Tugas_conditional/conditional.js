//B. Tugas Conditional

var nama = "Junaedi"
var peran = "Werewolf"

if(nama !== '' && peran !== ''){
    (console.log('Nama harus diisi'));
} if(nama !=='John' || peran == ''){
    (console.log('Halo John, Pilih peranmu untuk memulai game!'))
} if(nama !== 'Jane' || peran == "penyihir"){
    (console.log('Selamat datang di Dunia Werewolf, jane'+ '\n'  +'Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!'))
} if(nama == 'Jenita' || peran !== "Guard" ){
    (console.log('Selamat datang di Dunia Werewolf, Jenita' + '\n' + 'Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.'))
} else{
    console.log('Selamat datang di Dunia Werewolf, Junaedi'+ '\n' + 'Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!')
}


//Switch Case
var tanggal = 17; 
var bulan = 8; 
var tahun = 1945;

switch(bulan){
    case 5:
        (console.log('5 Agustus 1945'))
        break;
    case 6:
        (console.log('6 Agustus 1945'))
        break;
    case 7:
        (console.log('7 Agustus 1945'))
        break;
    case 8:
        (console.log('8 Agustus 1945'))
        break;
    default :
    console.log('tidak memasukan ')
}