//soal Node.1
console.log('no.1')
const golden = ()=>{
    console.log('this is golden!!')
}
golden()


//soal no.2
console.log('no.2')
const newFunction = function literal(firstName, lastName){
    return {
      firstName: firstName,
      lastName: lastName,
      fullName: function(){
        console.log(`${firstName + ' ' + lastName}`)
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 

//soal no.3
console.log('no.3')
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const {firstName:a,lastName:b,destination:c,occupation:d,spell:e} = newObject;
  console.log(a,b,c,d,e)

  //soal no.4
  const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
console.log(combined)
//es6
const gabungan = [...west, ...east];
console.log(gabungan)

//soal no.5
console.log('no.5')
const planet = "earth"
const view = "glass"
const lorem = "lorem"
const dolor = "dolor sit amet, consectetur adipiscing elit"
const tempor = "do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam"

// var before = 'Lorem ' + view + 'dolor sit amet, ' +  
//     'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +
//     'incididunt ut labore et dolore magna aliqua. Ut enim' +
//     ' ad minim veniam'
let before = `${lorem +' '+ view + ' ' + dolor + ' ' + planet +' '+ tempor}`;
//es6
console.log(before) 

