// Looping while
var angka = 2
var mulai = 2
var angka2= 20

console.log('LOOPING PERTAMA')
while(angka <= 20){
    if(angka%mulai == 0){
    console.log(angka+ '-' + ' I love coding')
    }
    angka++;
}

console.log('looping Kedua')
while(angka2>=2){
    if(angka2%mulai == 0){
    console.log(angka2+ '-' + 'I will become a mobile developer')
    }
    angka2--
}

// No. 2 Looping menggunakan for
var nilai = 2
var s = 3;
var kelipatanTiga = s**3;

console.log("OUTPUT")
var santai = "- santai"
var berkualitas = "- berkualitas"
var lovecoding = "- i love coding"
for(var i=1;i<=20;i++){
    if(i % 2 == 0){
        console.log(i + santai)
    } else if(i % 3 == 0){
        console.log(i + lovecoding)
    } else{
        console.log(i+ berkualitas)
    }
}

//No.3 Membuat Persegi Panjang
var pager = ''
for(var i=0;i<4;i++){
    for(var j=0;j<8;j++){
    pager += '#'
    }
    pager += '\n'
}
console.log(pager);

var tangga =''
for(var i=0;i<7;i++){
    for(var j=0;j<=i;j++){
    tangga += '#'
    }
    tangga += '\n'
}
console.log(tangga);

var catur = ''
for(var i=0;i<8;i++){
    for(var j=0;j<=i;j++){
    if(i%2 == 1){
        catur +=''
    }
    if(i%2 == 0){
        catur += '#'
    }
    }
}
console.log(catur);
