import React, { Component } from  'react';
import {StyleSheet,Text,View,Image} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const Login = () =>{
    return(
        <View style={styles.container}>
            <Image source={require('./images/logo.png')} style={styles.backgroundImage}  />
        </View>
    )
}

    let styles = StyleSheet.create({
        container: {
             flex: 1,
             backgroundColor: '#2980b9',
             height:null,
             weight:200
        },
        
             backgroundImage:{
                flex: 1,
                resizeMode: 'center', // or 'stretch',
                height:20,
                weight:600,
                justifyContent: "center",
                alignItems: "center",
           },
           
});

export default Login;