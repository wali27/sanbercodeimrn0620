//penandaan jawaban soal
console.log('soal no.1')
// Soal No. 1 (Range)
function range(startNum,finishNum,){
    if(!startNum || !finishNum){
        return -1;
    }
    let result = []
    let step = startNum <= finishNum ? 1 : -1;
    for(let i = startNum; step >=0? i <= finishNum :i >= finishNum; i+=step){
        result.push(i);
    }
    return result;
}
console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50))
console.log(range());

//penandaan jawaban soal
console.log('soal no.2')
//Soal No. 2 (Range with Step)
function rangeWithStep(startNum,finishNum,step){
    var result = []
    if(startNum < finishNum)
       for(var i=startNum;i<=finishNum;i+=step){
           result.push(i);
       }
    if(startNum > finishNum)
       for(var j=startNum;j>=finishNum;j-=step){
           result.push(j)
       }
       return result;
}
console.log(rangeWithStep(1,10,2))
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4)) 

//penandaan jawaban soal
console.log('soal no.3')
//Soal No. 3 (Sum of Range)
function sum(startNum, finishNum, step){
var result = 0;
var numArray = []
   
   if(step === undefined){
       numArray = range(startNum,finishNum)
   } else{
       numArray = rangeWithStep(startNum,finishNum,step)
   }
   if(startNum && !finishNum ){
    return startNum = 1
}
   for(var i=0; i < numArray.length; i++){
       result += numArray[i]
   }
   return result
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


//penandaan jawaban soal
console.log('jawaban soal no.4')
// Soal No. 4 (Array Multidimensi)
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

input.forEach(function arr(e){
    console.log(`Nomor ID : ${e[0]}`)
    console.log(`Nama : ${e[1]}`)
    console.log(`TTL : ${e[2]},${e[3]}`)
    console.log(`Hobi : ${e[4]}` + '\n')
})


//penandaan jawaban soal
console.log('jawaban soal no.5')
//Soal No. 5 (Balik Kata)
function balikKata(inputKata){
    var kata = '';
    var lengthKata = inputKata.length;
    for(lengthKata; lengthKata>=0; lengthKata--){
        var kal = inputKata[0,lengthKata];
        if(kata){
            kata = kata + kal;
        } else{
            kata = kal;
        }
    }
    return kata
}

console.log(balikKata("Kasur Rusak"))// kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//penandaan jawaban soal
console.log('jawaban soal 6')
//Soal No. 6 (Metode Array)
var data = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  
function dataHandling2(){
   data.splice(1,4,"Roman Alamsyah Elsharawy","Provinsi Bandar Lampung"
   ,"21/05/1989","Pria","SMA Internasional Metro")
   return data
}
console.log(dataHandling2())