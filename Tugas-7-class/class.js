// jawaban soal No.1 Animal Class
console.log('Soal No.1 Animal Class')

// 1.Animal Class
class Animal{
    constructor(nama){
         this._name = nama;
         this.hewan = nama;
    }
    kera(){
        return this.hewan + "berkaki 2" + ' '
    }
    frog(){
        return this.hewan + "berkaki 4" + ' '
    }
    get name(){
        return this._name;
    }
    get legs(){
        return this._legs;
    }
    get cold_blooded(){
        return this._cold_blooded;
    }
    set leg(a){
        this._legs = a;
    }
    set cold(c){
        this._cold_blooded = c;
    }
}

var sheep = new Animal("shaun");
sheep.leg = 4;
sheep.cold = false;

 // release 0
console.log('#Release 0')
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

//release 1
class Ape extends Animal{
    constructor(nama){
        super(nama);
    }
    yell(){
        return this.kera() + "Auoo"
    }
}

class Frog extends Animal{
    constructor(nama){
        super(nama);
    }
    jump(){
        return this.frog() + "hop hop"
    }
}

// release 1
console.log('#Release 1') 

var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"
 
var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop"

//soal no.2
// 2. Function to Class
class Clock{
    constructor({template}){
        this.template = template
        this.date = new Date();
        this.hours = this.date.getHours();
        this.mins = this.date.getMinutes();
        this.secs = this.date.getSeconds();
        this.output = template.replace('h', this.hours).replace('m', this.mins).replace('s', this.secs);
    }

    render(){
        var timer;
        if(this.hours < 10) hours = '0' + hours;
        if(this.mins < 10) mins = '0' + mins;
        if(this.secs < 10) secs = '0' + secs;
        console.log(this.output);
    }

    stop(){
        clearInterval(this.timer);
    }

    start(){
        this.render();
        this.timer = setInterval(() => this.render(),1000)
    }
}

var clock = new Clock({template: 'h:m:s'});
console.log(clock.start()); 
