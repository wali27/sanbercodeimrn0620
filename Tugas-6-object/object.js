//soal no.1
console.log('soal no.1')
//jawaban soal no.1
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]

var now = new Date()
var thisYear = now.getFullYear()
  
function arrayToObject(arr){
    var arrObj = {};
    for(var i=0 ; i < arr.length; i++){
        arrObj = {
            firstName : arr [i][0],
            lastname : arr [i][1],
            gender : arr [i][2],
            age : thisYear - arr [i][3]
        } 
        if(!arrObj.age || arrObj.age <=0){
            arrObj.age = "Invalid Birth Year"
        } 
        console.log(`${i+1}. ${arrObj.firstName} : {
            firstname : ${arrObj.firstName},
            lastname  : ${arrObj.lastname},
            gender    : ${arrObj.gender},
            age       : ${arrObj.age}
        }`)
    }
    return arr
}

console.log(arrayToObject(people))
console.log(arrayToObject(people2))


// soal no.2
console.log('jawaban soal no.2')
// jawaban soal no.2
function shoppingTime(memberId,money){
    var PriceList = {Sepatu_Staccatu: 1500000,Baju_Zoro: 500000,Baju_HN: 250000,Sweater_Uniklooh: 175000,casing_handphone: 50000}
    var purchaseList = []
    
    
    if(memberId == '' || memberId == null){
        return "mohon maaf, toko X hanya berlaku untuk member saja";
    } else if(money < 50000 && memberId !== ''){
        return "Mohon maaf, uang tidak cukup"
    } else {
        var uang = money
        for(var i in PriceList){
            if(uang >= PriceList[i]){
            uang -= PriceList[i]
            purchaseList.push(i);
            }
        }
          return object = {
                "memberId" : memberId,
                "money"    : money,
                "listPurchased" : purchaseList,
                "change"   : uang
            }
        }
    }

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//Soal No. 3 (Naik Angkot)
// jawaban soal no.3
console.log('soal no.3 (naik angkot')

function naikAngkot(arrPenumpang){
    rute = ['A','B','C','D','E','F'];
    var c = arrPenumpang.length;
    if(arrPenumpang == []){
        return "[]";
    } else{
        for(var i=0; i < arrPenumpang.length; i++){
            var berangkat = rute.indexOf(arrPenumpang[i][1]);
            var tujuan = rute.indexOf(arrPenumpang[1][2]);
            var ongkos = tujuan - berangkat;
            ongkos = ongkos*2000;
            var penumpang = {
                penumpang : arrPenumpang[i][0],
                naikDari : arrPenumpang[i][1],
                tujuan   : arrPenumpang[i][2],
                bayar    : ongkos
            }
            return penumpang;
        }
    }
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]